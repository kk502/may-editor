# MayEditor

# 线上体验地址：http://doc.maymay5.com

#### 介绍
AI在线文本编辑器

#### 软件架构
前端：Jquery,Markdown-it,wangEditor
后端：dotnet core 3.1 MVC（C#）

#### 安装教程

1.  使用Visual Studio打开项目，还原好各包（正常情况会自动还原）

2.  修改appsetting.json文件中的配置：OpenAIUrl(openai api请求地址)，APIkey(openai api key)，ChatModel(可根据key的权限自行设置模型，模型命名规范与openai官方一致，例如：“gpt-3.5-turbo-16k-0613”)

3.  编译

4.  双击运行可执行文件“MayEditor.exe”（非windows系统可使用终端，输入命名：“dotnet MayEditor.dll”）【注意，不论何系统，安装.net core3.1 SDK都是必要的执行环境】

5.  访问localhost:5000即可使用

#### 使用说明

1.  标准功能与wangEditor一致

2.  当【选中编辑器文字时】会弹出二级悬窗(wangEditor标准功能)，点击悬窗最后一个按钮【May妹帮帮忙】按钮，即可将选中文本数据提交给GPT，并书写你需要GPT执行的任务

3.  编辑完成后，点击【导出】按钮即可生成word

4.  已加入浏览器5秒缓存一次的机制，即便是关闭浏览器，您的文档编辑内容也不会被清除（在使用同一设备的相同浏览器恢复打开的时候）

![alt 图](https://i.mij.rip/2023/11/01/0f0150c50e0ac93c505dcba6011d07b2.png)

![alt 图](https://i.mij.rip/2023/11/01/c14696c4b15afea9da3d79ff2fcc9ea0.png)

![alt 图](https://i.mij.rip/2023/11/01/1408f014acfdd2f1dc8a5d2c30c4be18.png)

#### 参与贡献

1.   @KK-WPF 


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
