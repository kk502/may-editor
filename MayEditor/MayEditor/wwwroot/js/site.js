﻿function generateGUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }

    return (
        s4() +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        '-' +
        s4() +
        s4() +
        s4()
    );
}
$(function () {
    function saveEdit() {
        if (editor) {
            console.log(editor.getHtml());
            //写入缓存，key常量：MayEditor
            localStorage.setItem("MayEditor", editor.getHtml());
        }
    }
    if (localStorage.getItem("MayEditor") == null)
        setInterval(saveEdit, 5000);
    else if (localStorage.getItem("MayEditor") != '<p><br></p>') {
        var data = localStorage.getItem("MayEditor");
        editor.setHtml(data);
        setInterval(saveEdit, 5000);
    }
    else
        setInterval(saveEdit, 5000);
});
var connection = new signalR.HubConnectionBuilder()
    .withUrl("/chat")
    .withAutomaticReconnect() // 启用自动重连
    .build();
connection.onclose(function () {
    setTimeout(function () {
        connection.start()
            .then(function () {
                console.log('Connected to SignalR hub');
            })
            .catch(function (error) {
                console.log('Error connecting to SignalR hub: ' + error);
            });
    }, 1000);
});
var md = window.markdownit();
connection.start()
    .then(function () {
        console.log('Connected to SignalR hub');
    })
    .catch(function (error) {
        console.log('Error connecting to SignalR hub: ' + error);
    });
connection.on('ReceiveMessage', function (message) {
    message = JSON.parse(message);
    if (!message.isfinish) {
        //const json = JSON.parse(response);
        $("#chatbox").html(md.render(message.msg));
        hljs.highlightAll();
    }
    else {
        $("#chatbox").append("😊");
        $("#copyBtn").slideDown();
        $("#send").text("重试");
        $("#send").slideDown();
    }
});
function send(prompt, chatId) {
    connection.invoke('SendMessage', prompt, chatId)
        .then(function () {

        })
        .catch(function (error) {
            console.log('Error sending notification: ' + error);
        });
}